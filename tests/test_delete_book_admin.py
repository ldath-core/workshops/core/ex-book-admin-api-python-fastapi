from pytest import mark

from test_fixtures import *
from tests.test_book_admins import TestBookAdmins


@mark.usefixtures('client')
class TestDeleteBookAdmin:

    def test_should_delete_book_admin(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)

        # when
        response = client.delete(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s deleted' % id)

    def test_should_return_not_found_for_invalid_id(self, client):
        # given
        id = 10

        # when
        response = client.delete(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_error_response(response, 404, f'there is no documents with id {id}')

    def test_should_return_not_found_for_valid_id(self, client):
        # given
        id = '63f36b8271a01f8f5f384a07'

        # when
        response = client.delete(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_error_response(response, 404, f'there is no documents with id {id}')
