from pytest import mark

from test_fixtures import *
from tests.test_book_admins import TestBookAdmins


@mark.usefixtures('client')
class TestGetBookAdmin:

    def test_should_get_book_admin_with_all_fields(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_response(response, 200, 'admin')
        TestBookAdmins.check_response_content(
            response,
            id,
            'admin0@gmail.com',
            'password0',
            'firstname0',
            'lastname0',
            1)

    def test_should_get_book_admin_with_only_required_fields(self, client):
        # given
        book_admin = {
            'email': 'test@gmail.com',
            'password': 'test'}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # given
        id = response.json()['content']['id']

        # when
        response = client.get('/v1/admins/' + id)

        # then
        TestBookAdmins.check_response(response, 200, 'admin')
        TestBookAdmins.check_response_content(
            response,
            id,
            book_admin['email'],
            book_admin['password'])

    def test_should_return_not_found_for_invalid_id(self, client):
        # given
        id = 10

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_error_response(response, 404, f'there is no documents with id {id}')

    def test_should_return_not_found_for_valid_id(self, client):
        # given
        id = '63f36b8271a01f8f5f384a07'

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_error_response(response, 404, f'there is no documents with id {id}')
