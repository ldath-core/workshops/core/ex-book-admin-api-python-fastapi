from os import environ
from pathlib import Path

from bcrypt import hashpw, gensalt
from pymongo import MongoClient
from pytest import fixture
from fastapi.testclient import TestClient

from book_admin.config import AppConfig
from book_admin.service import Service


@fixture()
def app():
    config = get_app_config()
    db = MongoClient(
        'mongodb://%s:%s@%s/%s?authSource=%s' % (
            config.Mongodb.User,
            config.Mongodb.Password,
            config.Mongodb.Host,
            config.Mongodb.Database,
            config.Mongodb.AuthenticationDatabase),
        serverSelectionTimeoutMS=1000).get_database()

    # hash generation is time-consuming, so we set the same hash for each test admin
    password_hash = hashpw(bytes('password' + str(id), 'utf-8'), gensalt()).decode('utf-8')
    test_data = ({
        'email': 'admin%d@gmail.com' % id,
        'passwordHash': password_hash,
        'firstName': 'firstname' + str(id),
        'lastName': 'lastname' + str(id),
        'version': 1
    } for id in range(100))

    db.drop_collection('admins')
    db.admins.insert_many(test_data)

    yield app


@fixture()
def client(app):
    service = Service(get_app_config())
    return TestClient(service.app)


@fixture()
def client_with_invalid_db_config(app):
    app_config = get_app_config()
    app_config.Mongodb.Host = 'not.existing.com'
    service = Service(app_config)
    return TestClient(service.app)


@fixture()
def runner(app):
    return app.test_cli_runner()


def get_app_config() -> AppConfig:
    cfg_file_path = environ.get('CFG_FILE', default=Path(__file__).parent.parent.joinpath('secrets/local.env.yaml'))
    return AppConfig.parse_file(cfg_file_path)

