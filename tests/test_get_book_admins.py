from pytest import mark

from test_fixtures import *
from tests.test_book_admins import TestBookAdmins


@mark.usefixtures('client')
class TestGetBookAdmins:

    def test_should_get_all_book_admins(self, client):
        # when
        response = client.get('/v1/admins/')

        # then
        TestBookAdmins.check_response(response, 200, 'admins - skip: 0; limit: 10')
        TestGetBookAdmins.check_pagination(response, 100, 0, 10, 10)

    def test_should_get_limited_book_admins_number(self, client):
        # given
        skip = 0
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestBookAdmins.check_response(response, 200, 'admins - skip: %d; limit: %d' % (skip, limit))
        TestGetBookAdmins.check_pagination(response, 100, skip, limit, 2)

    def test_should_skip_first_page(self, client):
        # given
        skip = 1
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestBookAdmins.check_response(response, 200, 'admins - skip: %d; limit: %d' % (skip, limit))
        TestGetBookAdmins.check_pagination(response, 100, skip, limit, 2)

    def test_should_skip_all_book_admins(self, client):
        # given
        skip = 60
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestBookAdmins.check_response(response, 200, 'admins - skip: %d; limit: %d' % (skip, limit))
        TestGetBookAdmins.check_pagination(response, 100, skip, limit, 0)

    def test_should_return_error_for_negative_skip(self, client):
        # given
        skip = -1
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestGetBookAdmins.check_error_message(response)

    def test_should_return_error_for_negative_limit(self, client):
        # given
        skip = 0
        limit = -2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestGetBookAdmins.check_error_message(response)

    def test_should_return_error_for_invalid_skip(self, client):
        # given
        skip = 'invalid'
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestGetBookAdmins.check_error_message(response)

    def test_should_return_error_for_invalid_limit(self, client):
        # given
        skip = 0
        limit = 'invalid'
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestGetBookAdmins.check_error_message(response)

    def test_should_return_admin_with_given_email(self, client):
        # given
        email = 'admin0@gmail.com'
        arguments = {
            'email': email
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestGetBookAdmins.check_pagination(response, 100, 0, 10, 1)

    def test_should_return_not_found_for_email(self, client):
        # given
        email = 'notexists@gmail.com'
        arguments = {
            'email': email
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # then
        TestGetBookAdmins.check_pagination(response, 100, 0, 10, 0)

    @staticmethod
    def check_pagination(response, count, skip, limit, results_number):
        assert 'count' in response.json()['content']
        assert 'skip' in response.json()['content']
        assert 'limit' in response.json()['content']
        assert 'results' in response.json()['content']

        assert response.json()['content']['count'] == count
        assert response.json()['content']['skip'] == skip
        assert response.json()['content']['limit'] == limit
        assert len(response.json()['content']['results']) == results_number

    @staticmethod
    def check_error_message(response):
        assert 'errors' in response.json()
        assert 'message' in response.json()
        assert response.json()['message'] == 'please fill all the fields'
