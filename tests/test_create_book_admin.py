from re import search

from pytest import mark

from test_fixtures import *
from tests.test_book_admins import TestBookAdmins


@mark.usefixtures('client')
class TestCreateBookAdmins:

    def test_should_create_book_admin_with_required_fields(self, client):
        # given
        book_admin = {
            'email': 'test@gmail.com',
            'password': 'test'}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_response(response, 201, r'admin: [0-9a-f]+ created')
        assert 'id' in response.json()['content']
        assert bool(search(r'[0-9a-f]+', response.json()['content']['id']))

    def test_should_create_book_admin_with_all_fields(self, client):
        # given
        book_admin = {
            'email': 'test@gmail.com',
            'password': 'test',
            'firstName': 'Test',
            'lastName': 'Test',
            'version': 1}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_response(response, 201, r'admin: [0-9a-f]+ created')
        assert 'id' in response.json()['content']
        assert bool(search(r'[0-9a-f]+', response.json()['content']['id']))

    def test_should_reject_request_without_required_field(self, client):
        # given
        book_admin = {
            'password': 'test'}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_reject_request_with_invalid_value_type(self, client):
        # given
        book_admin = {
            'email': 'test@gmail.com',
            'password': []}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_reject_request_with_empty_field_value(self, client):
        # given
        book_admin = {
            'email': '',
            'password': 'test'}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_reject_request_with_invalid_version_value_type(self, client):
        # given
        book_admin = {
            'email': 'test@gmail.com',
            'password': 'test',
            'version': 'invalid'}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_reject_request_with_negative_version_value(self, client):
        # given
        book_admin = {
            'email': 'test@gmail.com',
            'password': 'test',
            'version': -1}

        # when
        response = client.post('/v1/admins/', json=book_admin)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')
