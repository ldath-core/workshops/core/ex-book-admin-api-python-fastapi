from pytest import mark

from test_fixtures import *
from tests.test_book_admins import TestBookAdmins


@mark.usefixtures('client')
class TestUpdateBookAdmin:

    def test_should_update_book_admin_email_and_password(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {
            'email': 'updated@gmail.com',
            'password': 'updated'}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'])

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_response(response, 200, 'admin')
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'])

    def test_should_update_book_admin_first_name(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {
            'email': 'updated@gmail.com',
            'password': 'updated',
            'firstName': 'Updated'}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'], first_name=patch['firstName'])

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_response(response, 200, 'admin')
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'], first_name=patch['firstName'])

    def test_should_update_book_admin_last_name(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {
            'email': 'updated@gmail.com',
            'password': 'updated',
            'lastName': 'Updated'}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'], last_name=patch['lastName'])

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_response(response, 200, 'admin')
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'], last_name=patch['lastName'])

    def test_should_update_book_admin_version(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {
            'email': 'updated@gmail.com',
            'password': 'updated',
            'version': 1}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'], version=patch['version'])

        # when
        response = client.get(f'/v1/admins/{id}')

        # then
        TestBookAdmins.check_response(response, 200, 'admin')
        TestBookAdmins.check_response_content(response, id, patch['email'], patch['password'], version=patch['version'])

    def test_should_reject_request_with_invalid_value_type(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'email': 1}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_reject_request_with_empty_field_value(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'password': ''}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_reject_request_with_unexpected_field(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'unexpected': 'unexpected'}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_error_response(response, 406, 'please fill all the fields')

    def test_should_return_not_found_for_invalid_id(self, client):
        # given
        id = 10
        patch = {'email': 'updated@gmail.com'}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_error_response(response, 404, f'there is no documents with id {id}')

    def test_should_return_not_found_for_valid_id(self, client):
        # given
        id = '63f36b8271a01f8f5f384a07'
        patch = {'email': 'updated@gmail.com'}

        # when
        response = client.put(f'/v1/admins/{id}', json=patch)

        # then
        TestBookAdmins.check_error_response(response, 404, f'there is no documents with id {id}')
