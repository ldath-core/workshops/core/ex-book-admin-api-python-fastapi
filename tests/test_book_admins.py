from re import search

import bcrypt
from httpx import Response
from pytest import mark


class TestBookAdmins:

    @staticmethod
    def check_response(response: Response, status, message_pattern):
        assert 'status' in response.json()
        assert 'message' in response.json()
        assert 'content' in response.json()

        assert response.status_code == status
        assert response.json()['status'] == status
        assert bool(search(message_pattern, response.json()['message']))

    @staticmethod
    def check_response_content(response, id, email, password, first_name='', last_name='', version=None):
        assert 'id' in response.json()['content']
        assert response.json()['content']['id'] == id

        assert 'email' in response.json()['content']
        assert response.json()['content']['email'] == email

        assert 'passwordHash' in response.json()['content']
        bcrypt.checkpw(str.encode(password), str.encode(response.json()['content']['passwordHash']))

        if first_name != '':
            assert 'firstName' in response.json()['content']
            assert response.json()['content']['firstName'] == first_name

        if last_name != '':
            assert 'lastName' in response.json()['content']
            assert response.json()['content']['lastName'] == last_name

        if version is not None:
            assert 'version' in response.json()['content']
            assert response.json()['content']['version'] == version

    @staticmethod
    def check_error_response(response: Response, status, message):
        assert 'status' in response.json()
        assert 'message' in response.json()
        assert 'errors' in response.json()

        assert response.status_code == status
        assert response.json()['status'] == status
        assert response.json()['message'] == message

    @staticmethod
    def get_admin_id(client):
        skip = 0
        limit = 1
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/admins/', params=arguments)

        # given
        return response.json()['content']['results'][0]['id']
