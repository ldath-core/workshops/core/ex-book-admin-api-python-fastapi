from os import environ

from pytest import mark

from test_fixtures import *


class TestHealth:

    @mark.usefixtures('client')
    def test_get_health(self, client):
        # when
        response = client.get('/v1/health/')

        # then
        TestHealth.check_response(response, 200, 'book admin api health')

        assert 'alive' in response.json()['content']
        assert 'mongo' in response.json()['content']

        assert response.json()['content']['alive']
        assert response.json()['content']['mongo']

    @mark.usefixtures('client_with_invalid_db_config')
    def test_get_health_with_invalid_db_config(self, client_with_invalid_db_config):
        # when
        response = client_with_invalid_db_config.get('/v1/health/')

        # then
        TestHealth.check_response(response, 200, 'book admin api health')

        assert 'alive' in response.json()['content']
        assert 'mongo' in response.json()['content']

        assert response.json()['content']['alive']
        assert not response.json()['content']['mongo']

    @staticmethod
    def check_response(response, status, message):
        assert 'status' in response.json()
        assert 'message' in response.json()
        assert 'content' in response.json()

        assert response.json()['status'] == status
        assert response.json()['message'] == message
