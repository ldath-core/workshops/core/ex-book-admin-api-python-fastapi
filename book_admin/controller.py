from typing import List

from fastapi import status
from fastapi.exceptions import RequestValidationError
from pydantic import NonNegativeInt, PositiveInt
from pydantic.error_wrappers import ValidationError
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError

from book_admin import crud
from book_admin.schema.admin import AdminRequest, AdminResponse, UpdateAdmin
from book_admin.schema.health import Health
from book_admin.schema.response import PaginatedContent
from book_admin.schema.response import Response as ResponseSchema


class AdminController:
    def __init__(self, database: MongoClient):
        self.database = database

    async def get_health(self) -> ResponseSchema[Health]:
        mongo_connected = True
        try:
            self.database.server_info()
        except ServerSelectionTimeoutError:
            mongo_connected = False

        return ResponseSchema[Health](
            status=status.HTTP_200_OK,
            message="book admin api health",
            content=Health(alive=True, mongo=mongo_connected))

    async def get_admins(self,
                         skip: NonNegativeInt = 0,
                         limit: PositiveInt = 10,
                         email: str = '') -> ResponseSchema[PaginatedContent[List[AdminResponse]]]:
        try:
            count = crud.count_admins(self.database.get_database())
            admins = crud.get_admins(self.database.get_database(), skip, limit, email)
        except ValueError as error:
            raise RequestValidationError(error) from error

        return ResponseSchema[PaginatedContent[List[AdminResponse]]](
            status=status.HTTP_200_OK,
            message=f"admins - skip: {skip}; limit: {limit}",
            content=PaginatedContent(count=count, skip=skip, limit=limit, results=admins))

    async def get_admin_by_id(self, admin_id: str) -> ResponseSchema[AdminResponse]:
        admin = crud.get_admin_by_id(self.database.get_database(), admin_id)
        return ResponseSchema[AdminResponse](
            status=status.HTTP_200_OK,
            message="admin",
            content=admin)

    async def create_admin(self, admin: AdminRequest) -> ResponseSchema[dict]:
        new_admin_id = crud.create_admin(self.database.get_database(), admin)
        return ResponseSchema[dict](
            status=status.HTTP_201_CREATED,
            message=f"admin: {new_admin_id} created",
            content={"id": new_admin_id})

    async def update_admin(self, admin_id: str, admin: UpdateAdmin) -> ResponseSchema[AdminResponse]:
        try:
            updated = crud.update_admin(self.database.get_database(), admin_id, admin)
        except ValidationError as error:
            raise RequestValidationError(error) from error
        return ResponseSchema[AdminResponse](
            status=status.HTTP_202_ACCEPTED,
            message=f"admin: {updated['id']} updated",
            content=updated)

    async def delete_admin(self, admin_id: str) -> ResponseSchema[dict]:
        crud.delete_admin(self.database.get_database(), admin_id)
        return ResponseSchema[dict](
            status=status.HTTP_202_ACCEPTED,
            message=f"admin: {admin_id} deleted",
            content={})
