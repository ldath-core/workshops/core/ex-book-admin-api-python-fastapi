from fastapi import APIRouter, status

from book_admin.controller import AdminController


def get_router(handler: AdminController):
    router = APIRouter(prefix="/v1")
    router.add_api_route("/health", handler.get_health, methods=["GET"])
    router.add_api_route("/admins", handler.get_admins, methods=["GET"],
                         response_model_exclude_none=True)
    router.add_api_route("/admins/{admin_id}", handler.get_admin_by_id, methods=["GET"],
                         response_model_exclude_none=True)
    router.add_api_route("/admins", handler.create_admin, status_code=status.HTTP_201_CREATED, methods=["POST"],
                         response_model_exclude_none=True)
    router.add_api_route("/admins/{admin_id}", handler.update_admin, status_code=status.HTTP_202_ACCEPTED, methods=["PUT"],
                         response_model_exclude_none=True)
    router.add_api_route("/admins/{admin_id}", handler.delete_admin, status_code=status.HTTP_202_ACCEPTED,
                         methods=["DELETE"],
                         response_model_exclude_none=True)
    return router
