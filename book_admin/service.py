from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from pymongo import MongoClient

from .config import PROJECT_TITLE, AppConfig
from .controller import AdminController
from .crud import AdminNotFoundException
from .exc_handler import no_admin_exception_handler, validation_exception_handler
from .router import get_router
from .middleware import JsonContentMiddleware


class Service:
    def __init__(self, config: AppConfig, enable_cors: bool = False):
        self.app = FastAPI(title=PROJECT_TITLE)
        self.config = config

        self.database = MongoClient(
            f'mongodb://{config.Mongodb.User}:{config.Mongodb.Password}@{config.Mongodb.Host}/{config.Mongodb.Database}'
            f'?authSource={config.Mongodb.AuthenticationDatabase}',
            serverSelectionTimeoutMS=1000)

        self.handler = AdminController(self.database)
        router_v1 = get_router(self.handler)
        self.app.include_router(router_v1)

        # add generic exception handler for AdminNotFoundException and ValidationError
        self.app.add_exception_handler(AdminNotFoundException, no_admin_exception_handler)
        self.app.add_exception_handler(RequestValidationError, validation_exception_handler)

        self.app.add_middleware(JsonContentMiddleware)

        if enable_cors:
            self.app.add_middleware(
                CORSMiddleware,
                allow_origins=["*"],
                allow_credentials=True,
                allow_methods=["*"],
                allow_headers=["*"],
            )

    def db_migrate(self):
        """
        For MongoDB it is not required for such operation.
        However, it may be implemented for advanced usage.
        """
        raise NotImplementedError

    def db_load(self):
        """
        For MongoDB it is not required for such operation.
        However, it may be implemented for advanced usage.
        """
        raise NotImplementedError
