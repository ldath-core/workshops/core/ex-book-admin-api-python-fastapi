from typing import Optional

from pydantic import BaseModel, Field
from pydantic_yaml import YamlModel

PROJECT_TITLE = "ex-book-admin-api"


class LoggerConfig(BaseModel):
    Level: str = Field(alias="level", default="info")


class MongoConfig(BaseModel):
    User: str = Field(alias='user')
    Password: str = Field(alias='password')
    Host: str = Field(alias='host')
    Database: str = Field(alias='database')
    AuthenticationDatabase: str = Field(alias='authentication-database')


class ServerConfig(BaseModel):
    Host: Optional[str] = Field(alias='host')
    Port: Optional[int] = Field(alias='port')


class AppConfig(YamlModel):
    Env: str = Field(alias='env')
    Server: Optional[ServerConfig] = Field(alias='server')
    Logger: LoggerConfig = Field(alias='logger')
    Mongodb: MongoConfig = Field(alias='mongodb')
