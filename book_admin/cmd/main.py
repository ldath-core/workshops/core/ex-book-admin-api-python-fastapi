#!/usr/bin/env python3

import argparse
import sys
from pathlib import Path

import uvicorn

from book_admin.config import AppConfig
from book_admin.logger import get_log_config
from book_admin.service import Service

parser = argparse.ArgumentParser(prog="ex-book-admin-api-python-fastapi")

parser.add_argument(
    "--config",
    help="config file",
    default=Path(__file__).parent.parent.parent.joinpath('secrets/local.env.yaml'))

parser.add_argument(
    "--bind", "-b",
    default="127.0.0.1",
    help="This flag sets the IP to bind for our API server - overwrites config")
parser.add_argument(
    "--port", "-p",
    default=8000,
    help="This flag sets the port of our API server - overwrites config",
    type=int)
parser.add_argument(
    "--cors", "-c",
    action=argparse.BooleanOptionalAction,
    help="This decide if we should enable cors - in the Prod NGiNX is solving cors for us")


def main():
    args = parser.parse_args()
    config = AppConfig.parse_file(args.config)

    service = Service(config, enable_cors=args.cors)
    log_config = get_log_config(config.Logger.Level.upper())
    uvicorn.run(service.app, host=args.bind,
                port=args.port,
                log_level=config.Logger.Level,
                log_config=log_config)
    return 0


if __name__ == "__main__":
    sys.exit(main())
