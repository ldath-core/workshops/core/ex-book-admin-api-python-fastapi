from typing import Optional

from pydantic import BaseModel, Field, EmailStr
from pydantic.main import ModelMetaclass


class AdminBase(BaseModel):
    email: EmailStr = Field(title="Admin email")
    firstName: str | None = Field(min_length=1, title="Admin first name", max_length=300)
    lastName: str | None = Field(min_length=1, title="Admin last name", max_length=300)
    version: int | None = Field(title="Admin definition version", default=1)

    class Config:
        orm_mode = True


class AdminRequest(AdminBase):
    password: str = Field(min_length=1, title="Admin password", max_length=300)


class AdminResponse(AdminBase):
    id: str | None = Field(title="Admin id")
    passwordHash: str = Field(min_length=1, title="Admin password hash", max_length=300)


class AllOptional(ModelMetaclass):
    """Makes model fields to be all optional"""
    def __new__(cls, name, bases, namespaces, **kwargs):
        annotations = namespaces.get('__annotations__', {})
        for base in bases:
            annotations.update(base.__annotations__)
        for field in annotations:
            if not field.startswith('__'):
                annotations[field] = Optional[annotations[field]]
        namespaces['__annotations__'] = annotations
        return super().__new__(cls, name, bases, namespaces, **kwargs)


class UpdateAdmin(AdminRequest, metaclass=AllOptional):
    pass
