from pydantic import BaseModel


class Health(BaseModel):
    alive: bool
    mongo: bool
