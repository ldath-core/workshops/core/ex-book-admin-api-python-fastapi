from typing import TypeVar, Generic

from pydantic.generics import GenericModel

DataTypeT = TypeVar('DataTypeT')


class Error(GenericModel, Generic[DataTypeT]):
    status: int
    message: str
    errors: DataTypeT
