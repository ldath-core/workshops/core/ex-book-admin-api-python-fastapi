from typing import List

from bcrypt import hashpw, gensalt
from bson import ObjectId
from bson.errors import InvalidId
from pymongo import ASCENDING
from pymongo.database import Database

import book_admin.schema.admin as schema
from book_admin.converter import convert_book_admin_entity_to_dto
from book_admin.validator import is_book_admin_input_valid


class AdminNotFoundException(Exception):
    pass


def count_admins(db_session: Database) -> int:
    return db_session.admins.count_documents({})


def get_admins(db_session: Database, skip: int = 0, limit: int = 100, email: str = '') -> List[dict]:
    if email == '':
        admins = db_session.admins.find()
    else:
        admins = db_session.admins.find({'email': email})

    return [
        convert_book_admin_entity_to_dto(admin)
        for admin in admins.sort('_id', ASCENDING).skip(skip * limit).limit(limit)]


def get_admin_by_id(db_session: Database, admin_id: str) -> dict:
    try:
        object_id = ObjectId(admin_id)
    except InvalidId as error:
        raise AdminNotFoundException(f'there is no document with id {admin_id}') from error

    admin = db_session.admins.find_one({'_id': object_id})

    if admin is None:
        raise AdminNotFoundException(f'there is no document with id {admin_id}')

    return convert_book_admin_entity_to_dto(admin)


def create_admin(db_session: Database, admin_dto: schema.AdminRequest) -> str:
    is_book_admin_input_valid(admin_dto)

    admin_entity = {
        'email': admin_dto.email,
        'passwordHash': compute_password_hash(admin_dto.password)}

    if admin_dto.firstName:
        admin_entity['firstName'] = admin_dto.firstName
    if admin_dto.lastName:
        admin_entity['lastName'] = admin_dto.lastName
    if admin_dto.version:
        admin_entity['version'] = admin_dto.version

    insert_one_result = db_session.admins.insert_one(admin_entity)
    return str(insert_one_result.inserted_id)


def delete_admin(db_session: Database, admin_id: str):
    try:
        object_id = ObjectId(admin_id)
    except InvalidId as error:
        raise AdminNotFoundException(f'there is no document with id {admin_id}') from error

    admin = db_session.admins.find_one({'_id': object_id})
    if admin is None:
        raise AdminNotFoundException(f'there is no document with id {admin_id}')

    db_session.admins.delete_one({'_id': object_id})


def update_admin(db_session: Database, admin_id: str, updates: schema.UpdateAdmin) -> dict:
    try:
        object_id = ObjectId(admin_id)
    except InvalidId as error:
        raise AdminNotFoundException(f'there is no document with id {admin_id}') from error

    admin_dict = db_session.admins.find_one({'_id': object_id})
    if admin_dict is None:
        raise AdminNotFoundException(f'there is no document with id {admin_id}')

    admin_fields = updates.dict(exclude_none=True)
    admin_dict.update(admin_fields)
    # revalidate admin schema
    schema.AdminRequest.validate(admin_dict)

    if 'password' in admin_dict:
        admin_dict['passwordHash'] = compute_password_hash(admin_dict['password'])
        del admin_dict['password']

    db_session.admins.update_one({'_id': object_id}, {'$set': admin_dict})

    return convert_book_admin_entity_to_dto(db_session.admins.find_one({'_id': object_id}))


def compute_password_hash(password: str) -> str:
    return hashpw(bytes(password, 'utf-8'), gensalt()).decode('utf-8')
