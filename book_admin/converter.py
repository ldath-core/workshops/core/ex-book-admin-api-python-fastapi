def convert_book_admin_entity_to_dto(entity: dict) -> dict:
    dto = entity.copy()

    dto['id'] = str(dto['_id'])
    del dto['_id']

    return dto
