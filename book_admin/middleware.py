from fastapi import Request
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint


class JsonContentMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint):
        headers_copy = request.headers.mutablecopy()
        headers_copy['content-type'] = 'application/json; charset=UTF-8'
        request.scope.update(headers=headers_copy.raw)
        return await call_next(request)
