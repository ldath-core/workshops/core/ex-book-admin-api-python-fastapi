
from fastapi import Request, status
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError

from book_admin.crud import AdminNotFoundException
from book_admin.schema.error import Error


async def no_admin_exception_handler(_: Request, ex: AdminNotFoundException) -> JSONResponse:
    return JSONResponse(dict(Error[list](status=status.HTTP_404_NOT_FOUND,
                                         message=str(ex),
                                         errors=[])),
                        status_code=status.HTTP_404_NOT_FOUND)


async def validation_exception_handler(_: Request, __: RequestValidationError) -> JSONResponse:
    return JSONResponse(dict(Error[list](status=status.HTTP_406_NOT_ACCEPTABLE,
                                         message="please fill all the fields",
                                         errors=[])),
                        status_code=status.HTTP_406_NOT_ACCEPTABLE)
