from typing import Any

from fastapi.exceptions import RequestValidationError

from book_admin.schema.admin import AdminRequest


def is_book_admin_input_valid(request_body: AdminRequest):
    has_request_body_non_empty_string_field(request_body.email, 'email')
    has_request_body_non_empty_string_field(request_body.password, 'password')

    if request_body.firstName:
        has_request_body_non_empty_string_field(request_body.firstName, 'firstName')
    if request_body.lastName:
        has_request_body_non_empty_string_field(request_body.lastName, 'lastName')
    if request_body.version:
        has_request_body_positive_integer_field(request_body.version, 'version')


def is_book_admin_input_update_valid(request_body: dict):
    allowed_fields = ('email', 'password', 'firstName', 'lastName', 'version')

    for field in allowed_fields:
        if field in request_body:
            has_request_body_non_empty_string_field(request_body, field)

    for field in request_body.keys():
        if field not in allowed_fields:
            good_keys = ', '.join(allowed_fields)
            raise RequestValidationError(f"only keys: {good_keys} are allowed")


def has_request_body_non_empty_string_field(request_body_field: Any, field_name: str):
    if not isinstance(request_body_field, str):
        raise RequestValidationError(field_name + ' must be string')
    if request_body_field == '':
        raise RequestValidationError(field_name + ' must not be empty')


def has_request_body_positive_integer_field(request_body_field: Any, field_name: str):
    if not isinstance(request_body_field, int):
        raise RequestValidationError(field_name + ' must be integer')
    if request_body_field <= 0:
        raise RequestValidationError(field_name + ' must be positive integer')
