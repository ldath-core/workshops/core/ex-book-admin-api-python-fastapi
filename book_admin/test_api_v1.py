from os import environ

from fastapi.testclient import TestClient

from .config import AppConfig
from .service import Service


CFG_FILE = environ.get("CFG_FILE", default="./secrets/local.env.yaml")

config = AppConfig.parse_file(CFG_FILE)
service = Service(config)
client = TestClient(service.app)


def test_get_health():
    pass
