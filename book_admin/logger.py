import logging


def get_log_config(level: int = logging.INFO):
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '{"level": "%(levelname)s",'
                '"msg": "%(message)s",'
                '"time": "%(asctime)s"}',
            },
        },
        'handlers': {
            'default': {
                'level': level,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': 'INFO',
                'propagate': True
            },
        }
    }
