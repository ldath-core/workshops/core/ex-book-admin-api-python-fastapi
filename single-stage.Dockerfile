FROM python:3.11-slim-bullseye
WORKDIR /ex-book-admin-api
COPY . .

RUN \
    apt-get update && \
    apt-get -y install libpq-dev gcc && \
    PBR_VERSION=1.2.3 pip3 install . && \
    apt-get clean

EXPOSE 8080

ENTRYPOINT ["ex-book-admin-api-python-fastapi"]

CMD ["--config", "/secrets/local.env.yaml", "serve", "-b", "0.0.0.0", "-p", "8080", "-m"]
